package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}
	
	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() { 
		SLLStack<Character> stack = new SLLStack<Character>(); 

		for (int i=0; i < s.length(); i++) { 
			char c = s.charAt(i); 
			if (Character.isLetter(c))
			   if (Character.isUpperCase(c))
				  stack.push(c); 
			   else if (stack.isEmpty())
					 return false; 
				   else {
					 char t = stack.top(); 
					 if (t == Character.toUpperCase(c))
					    stack.pop();  
					 else 
					    return false; 
					}
				else 
					return false; 
		}
		
		return stack.isEmpty(); // if stack is empty, all chars connected.
	}
	
	public String toString() { 
		return s; 
	}

	/** If �s� is a valid symmetric String, this method returns
    	a parenthesized expression from the content in �s�.
 		Such expression is one that contains all the elements
 		in the string, but where each symmetric substring in it
		is enclosed within < and >. 
	 **/

	public String parenthesizedExpression() {
		
		SLLStack<Character> stack = new SLLStack<Character>(); 
		
		if(!this.isValidContent())
			return null;
		
		for (int i = s.length()-1; i >= 0; i--) {
			char c = s.charAt(i);
			
			if(Character.isUpperCase(c)) {
				stack.push(c);
				stack.push('<');
			}
			else {
				stack.push('>');
				stack.push(c);
			}
		}		
		
		int size = stack.size();
		String s = "";
		
		for (int i = 0; i < size; i++) {
			if(i % 2 == 0)
				s = s.concat(" ");
				
			s = s.concat(stack.pop() + "");
		}
		
		return s;
	}
//	throws StringIsNotSymmetricException 
//	{
//		
//		
//		return null;  // need to change if necessary....
//	}
}